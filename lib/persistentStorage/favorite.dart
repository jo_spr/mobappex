///
/// Created by Jonatan on 09.06.2020.
///
import 'userdata.dart' as userdata;

class Favorite{
  String _nameValue;
  double _backValue, _middleValue, _feetValue;
  Favorite(this._nameValue, this._backValue, this._middleValue, this._feetValue);

  Favorite.fromJson(Map<String, dynamic> json)
    : _nameValue = json[userdata.favoriteName],
      _backValue = json[userdata.favoriteBackValue],
      _middleValue = json[userdata.favoriteMiddleValue],
      _feetValue = json[userdata.favoriteFeetValue];

  Map<String, dynamic> toJson() => {
    userdata.favoriteName: _nameValue,
    userdata.favoriteBackValue: _backValue,
    userdata.favoriteMiddleValue: _middleValue,
    userdata.favoriteFeetValue: _feetValue,
  };

  String get name => this._nameValue;
  double get back => this._backValue;
  double get middle => this._middleValue;
  double get feet => this._feetValue;
}
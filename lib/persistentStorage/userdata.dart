///
/// Created by Jonatan on 09.06.2020.
///
/// used to keep unified identificators for values stored in sharedPreferences
///

  const String backRestValue = 'BedBackAngle';
  const String middleRestValue = 'BedMidAngle';
  const String feetRestValue = 'BedLegAngle';
  const String selectedColor = 'LightColor';
  const String lightLevel = 'LightLevel';
  const String volumeMusic = 'VolumenLevel';
  
  //favorite intern
  const String favoriteName = 'favoriteName';
  const String favoriteBackValue = 'favoriteBackValue';
  const String favoriteMiddleValue = 'favoriteMiddleValue';
  const String favoriteFeetValue = 'favoriteFeetValue';

  //favorite object names
  const String favoriteOne = 'favoriteOne';
  const String favoriteTwo = 'favoriteTwo';
  const String favoriteThree = 'favoriteThree';
  const String favoriteFour = 'favoriteFour';
  const String favoriteFive = 'favoriteFive';
  const String favoriteSix = 'favoriteSix';

  //app values
  const String language = 'language';
  const String theme = 'theme';
  

  //languageData
  const String languageEN = 'en';
  const String languageDE = 'de';
  const String languageES = 'es';

  //themeData
  const String themeLight = 'light';
  const String themeDark = 'dark';
  const String themeSnooze = 'snooze';

  //routes
  const String routeSettings = '/settings';
  const String routeSettingsLang = '/settings_lang';
  const String routeSettingsTheme = '/settings_theme';
  const String routeAccountDetails = '/account_details';
  const String routeStartTutorial = '/start_tutorial';
  const String routeSupport = '/support';

///
/// Created by Jonatan on 06.05.2020.
///
/// info: Tablet specs: 10.1" (1920*1200p) 
///         (in AVD: Nexus7 has same resolution)
///
import 'package:flutter/material.dart';
import 'app.dart';

void main() => runApp(App());

/*TODO:{
#    tabview mit southbar ansteuern
#      southbar index bei physic cntrl updaten
#    widgets in eigene dart files schieben
#    layoutarbeiten - padding undso
#    seitenmenu mit profilepic als icon
#    seitenmenu mit accountsettings und anderen menus
#    appcontainer fullscreen
#    seitenmenu mit contents füllen -> next slide contents (settings:[language, theme,..])
#    internationalization einbauen

    contents erstellen:
#      auf #2#3 jeweils button unten rechts mit continue


      //home#1 (TODO: Cyril)
#      großer breiter button unten 'Snoozen'
#        -> tap: _navigateTo(aura)
#          -> button animateTo right bottom corner (text: continue)

      //aura#2 (TODO: Cyril)
      colorpicker -> farbcode als rgb wert übergeben
      presets als cards mit grafiken
      presets: [Strand, Dschungel, Wald, Berge, Meer]

#      //sitzeinstellungen#3 (TODO: JONI)
#      globaler stoppknopf
#      wenn preset ausgewählt wird, animateTo mit kondensator funktion
#      sitzposition animieren ->
#            3 versch. lines
#              wenn change, dann rot makieren
#      mgmtfavorite -> tapOnStar
#        dialog 'please enter name' -> save -> new choicechip 
#        wenn schon favorite, dann favorite choicechip delete und unten snackbar mit UNDO
#          (max 3 custom favorites [insgesamt 6]) (wenn user neu ist müssen erste drei positionen in db gespeichert werden)
api              (im endeffekt werden id 1-3 vorgegeben und erst überschrieben wenn sie gelöscht werden)
#      sitzanimation mit gradient oder point im winkel

      //musik#4 (TODO: Cyril)
      auswahl von standardkrams, spotify, cast
      wifi direct
      bluetooth
      Spotify does offer an Android SDK (https://developer.spotify.com/documentation/android/quick-start/) that lets you play a playlist.
      You would have to figure out a way to handle authentication with Spotify on the tablet and a way to transmit the Playlist ID from the user's device to the tablet.

      Another possible way would be to create a Wifi Direct connection between the two devices and start a live audio stream on the users's device.
      https://medium.com/@ssaurel/implement-audio-streaming-in-android-applications-8758d3bc62f1


#      code beautify => resourcendatei für lang(/), colors, themes
#      daten persistent speichern -> sharedPreferences
#        lang bei init rausziehen, bei auswahl selecten
#      stop button form octagon
#      verschieden themes :  snooze 56, 199, 239
#                            dark theme
#                            ligth theme
#            anwenden auf menubar, bottomnavbar, 
      tutorial popup
      doku schreiben
api   api anbinden mit farbe select, seat position, music playback
#     restzeit anzeigen die der user noch in der kapsel hat
api     -> drei minuten vor ende licht hochfahren
      schriftart huebsch
api      wenn user angemeldet wird, alte preferences übernehmen
      wenn user abgemeldet wird, standardwerte anfahren
#      temperatur steuern (vllt aura page)
      review bei nächster abgabe mit pdf

*/









/* default new app

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
*/
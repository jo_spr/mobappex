// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "accountDetails" : MessageLookupByLibrary.simpleMessage("Account"),
    "add" : MessageLookupByLibrary.simpleMessage("Hinzufügen"),
    "addFavorite" : MessageLookupByLibrary.simpleMessage("Sitzposition zu Favoriten hinzufügen"),
    "aura" : MessageLookupByLibrary.simpleMessage("Aura"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Abbrechen"),
    "continueButton" : MessageLookupByLibrary.simpleMessage("Weiter"),
    "dark" : MessageLookupByLibrary.simpleMessage("Dunkel"),
    "deletedFavorite" : MessageLookupByLibrary.simpleMessage("Favorit entfernt"),
    "heyWorld" : MessageLookupByLibrary.simpleMessage("Hallo Wetl"),
    "home" : MessageLookupByLibrary.simpleMessage("Start"),
    "language" : MessageLookupByLibrary.simpleMessage("Sprache"),
    "leave" : MessageLookupByLibrary.simpleMessage("Jetzt verlassen"),
    "leaveConfirm" : MessageLookupByLibrary.simpleMessage("Willst du jetzt sofort gehen?"),
    "light" : MessageLookupByLibrary.simpleMessage("Hell"),
    "music" : MessageLookupByLibrary.simpleMessage("Musik"),
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "ok" : MessageLookupByLibrary.simpleMessage("OK"),
    "seat" : MessageLookupByLibrary.simpleMessage("Sitz"),
    "sessionEnd" : MessageLookupByLibrary.simpleMessage("Deine Sitzung ist kurz vor dem Ende"),
    "settings" : MessageLookupByLibrary.simpleMessage("Einstellungen"),
    "snooze" : MessageLookupByLibrary.simpleMessage("Snooze"),
    "startSnoozing" : MessageLookupByLibrary.simpleMessage("Snoozen"),
    "startTutorial" : MessageLookupByLibrary.simpleMessage("Starte Tutorial"),
    "storeUpToFavorites" : MessageLookupByLibrary.simpleMessage("Du kannst nur bis zu 6 Favoriten speichern"),
    "support" : MessageLookupByLibrary.simpleMessage("Hilfe"),
    "thankYou" : MessageLookupByLibrary.simpleMessage("Vielen Dank, dass du dich heute für uns entschieden hast"),
    "theme" : MessageLookupByLibrary.simpleMessage("Theme"),
    "tutorial" : MessageLookupByLibrary.simpleMessage("Tutorial"),
    "undo" : MessageLookupByLibrary.simpleMessage("Rückgängig"),
    "yes" : MessageLookupByLibrary.simpleMessage("Ja")
  };
}

///
///Created by Jonatan on 21.05.2020.
///
import 'dart:async';
import 'package:Snooze/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:Snooze/persistentStorage/userdata.dart' as userdata;

class SideMenuDrawer extends StatefulWidget {
  SideMenuDrawer({Key key, this.endTime}) : super(key: key);
  final DateTime endTime;

  @override
  _SideMenuDrawerState createState() => _SideMenuDrawerState();
}

class _SideMenuDrawerState extends State<SideMenuDrawer> {

  String _username = 'username';
  String _email = 'user@fra-uas.de';

  String _imageUrl = 'https://i.imgur.com/BoN9kdC.png';


  _onAccountDetailsTap(BuildContext context) {
    Navigator.pop(context);
    Navigator.pushNamed(context, userdata.routeAccountDetails);
  }

  _onTutorialStart(BuildContext context){
    Navigator.pop(context);
    Navigator.pushNamed(context, userdata.routeStartTutorial);
  }

  _onSupportTap(BuildContext context){
    Navigator.pop(context);
    Navigator.pushNamed(context, userdata.routeSupport);
  }

  _onSettingsTap(BuildContext context){
    Navigator.pop(context);
    Navigator.pushNamed(context, userdata.routeSettings);
  }

  _onLeaveTap(BuildContext context){
    Navigator.pop(context);
    _confirmLeave(context).then((onValue) {
      if(onValue!=null && onValue==true){
        _logout();
      }
    });
  }


  _logout() async {
    print('sidemenu._logout(): logging out');
    //deleting all sharedPreferences
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    _timer?.cancel();

    //TODO: #api call logout
  }


  //add favorite dialog
  Future<bool> _confirmLeave(BuildContext context){
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text(AppLocalization.of(context).leave),
        content: new Text(AppLocalization.of(context).leaveConfirm),
        actions: <Widget> [
          OutlineButton(
            child: Text(AppLocalization.of(context).yes),
            onPressed: () => Navigator.of(context).pop(true),
          ),
          OutlineButton(
            child: Text(AppLocalization.of(context).cancel),
            onPressed: () => Navigator.of(context).pop(false),
          ),
        ],
      );
    });
  }




  Timer _timer;
  DateTime endTime;

  @override
  initState(){
    super.initState();
    endTime = widget.endTime;
    if(endTime.difference(DateTime.now()).inSeconds>0){
      _timer = Timer.periodic(Duration(seconds: 1), (_timer) => setState((){}));
    }
  }

  @override
  dispose() {
    _timer?.cancel();
    super.dispose();
  }

  //return remaining time as formatted String
  String _remainingTime() {
    Duration remainingDuration = endTime.difference(DateTime.now());
    if(remainingDuration.inSeconds<=0){
      _timer?.cancel();
      return '00:00:00';
    }

    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(remainingDuration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(remainingDuration.inSeconds.remainder(60));
    return '${twoDigits(remainingDuration.inHours)}:$twoDigitMinutes:$twoDigitSeconds';
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(_username),
            accountEmail: Text(_email),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.grey[700],
              radius: 52.0,
              child: Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(_imageUrl)))),
            ),
          ),
          ListTile(
            leading: Icon(Icons.account_box),
            title: Text(AppLocalization.of(context).accountDetails),
            onTap: () => _onAccountDetailsTap(context),
          ),
          ListTile(
            leading: Icon(Icons.help),
            title: Text(AppLocalization.of(context).startTutorial),
            onTap: () => _onTutorialStart(context),
          ),
          ListTile(
            leading: Icon(Icons.message),
            title: Text(AppLocalization.of(context).support),
            onTap: () => _onSupportTap(context),
          ),
          ListTile(
            leading: Icon(Icons.av_timer),
            title: new Text(_remainingTime()),
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: new Text(AppLocalization.of(context).leave),
            onTap: () => _onLeaveTap(context),
          ),

          //spacer
          Expanded(
            child: Container(),
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text(
              AppLocalization.of(context).settings,
            ),
            onTap: () => _onSettingsTap(context),
          ),
        ],
      ),
    );
  }
}

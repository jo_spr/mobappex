///
/// Created by Jonatan on 21.05.2020.
///
import 'package:Snooze/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:Snooze/bloc/themeChanger.dart';
import 'package:Snooze/persistentStorage/userdata.dart' as userdata;

enum ThemeEnum{light, dark}//, snooze}

class SettingsThemePage extends StatefulWidget {
  SettingsThemePage({Key key}) : super(key: key);

  @override
  _SettingsThemePageState createState() => _SettingsThemePageState();
}

class _SettingsThemePageState extends State<SettingsThemePage> {

  //default theme
  ThemeEnum _theme = ThemeEnum.light;

  ThemeChanger _themeChanger;

  @override
  initState(){
    super.initState();
    _loadTheme();
  }

  _loadTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String themeString = prefs.getString(userdata.theme);
    if(themeString!=null && themeString.isNotEmpty){
      switch(themeString){
        case userdata.themeLight:   _onSelect(ThemeEnum.light);
                                    return;
        case userdata.themeDark:    _onSelect(ThemeEnum.dark);
                                    return;
        // case userdata.themeSnooze:  _onSelect(ThemeEnum.snooze);
        //                             break;
      }
    }
    _onSelect(_theme);
  }

  _onSelect(ThemeEnum value){
    setState(() {
      this._theme = value;
      switch(value){
        case ThemeEnum.light:   _themeChanger.setTheme(userdata.themeLight);
                                break;
        case ThemeEnum.dark:    _themeChanger.setTheme(userdata.themeDark);
                                break;
        // case ThemeEnum.snooze:  _themeChanger.setTheme(userdata.themeSnooze);
        //                         break;
      }
      _updateTheme();
    });
  }

  _updateTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(userdata.theme, _themeChanger.getThemeName);
  }

  @override
  Widget build(BuildContext context){
    _themeChanger = Provider.of<ThemeChanger>(context);
    return new ListView(
      children: [
        // new RadioListTile <ThemeEnum>(
        //   title: Text(AppLocalization.of(context).snooze),
        //   value: ThemeEnum.snooze,
        //   groupValue: _theme,
        //   onChanged: _onSelect,
        // ),
        new RadioListTile <ThemeEnum>(
          title: Text(AppLocalization.of(context).light),
          value: ThemeEnum.light,
          groupValue: _theme,
          onChanged: _onSelect,
        ),
        new RadioListTile <ThemeEnum>(
          title: Text(AppLocalization.of(context).dark),
          value: ThemeEnum.dark,
          groupValue: _theme,
          onChanged: _onSelect,
        ),
      ],
    );
  }
}

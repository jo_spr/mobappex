///
/// Created by Jonatan on 19.05.2020.
///

/*
 * 
 * https://flutter.dev/docs/development/accessibility-and-localization/internationalization
 *
 * flutter pub run intl_translation:extract_to_arb --output-dir=lib/l10n lib/locale/app_localization.dart
 * flutter pub run intl_translation:generate_from_arb --output-dir=lib/l10n --no-use-deferred-loading .\lib\l10n\intl_messages.arb .\lib\l10n\intl_en.arb .\lib\l10n\intl_de.arb .\lib\l10n\intl_es.arb .\lib\locale\app_localization.dart
 */

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import '../l10n/messages_all.dart';
import 'package:Snooze/persistentStorage/userdata.dart' as userdata;

class AppLocalization {
  static Future<AppLocalization> load(Locale locale) {
    final String name =
        locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return AppLocalization();
    });
  }

  static AppLocalization of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }

  // list of locales
  String get heyWorld => Intl.message(
      'Hey World',
      name: 'heyWorld',
      desc: 'Simpel word for greeting ',
    );

  String get settings => Intl.message(
      'Settings',
      name: 'settings',
      desc: 'change preferences',
    );
  String get home => Intl.message(
      'Home',
      name: 'home',
    );
  String get continueButton => Intl.message(
      'Continue',
      name: 'continueButton',
    );
  String get aura => Intl.message(
      'Aura',
      name: 'aura',
    );
  String get seat => Intl.message(
      'Seat',
      name: 'seat',
    );
  String get music => Intl.message(
      'Music',
      name: 'music',
  );
  String get accountDetails => Intl.message(
      'Account',
      name: 'accountDetails',
    );
  String get support => Intl.message(
      'Support',
      name: 'support',
    );
  String get startTutorial => Intl.message(
      'Start tutorial',
      name: 'startTutorial',
  );
  String get theme => Intl.message(
      'Theme',
      name: 'theme',
    );
  String get language => Intl.message(
      'Language',
      name: 'language',
    );
  String get tutorial => Intl.message(
      'Tutorial',
      name: 'tutorial',
    );
  String get startSnoozing => Intl.message(
      'Snooze',
      name: 'startSnoozing',
    );
  String get addFavorite => Intl.message(
      'Add position as favorite',
      name: 'addFavorite',
    );
  String get cancel => Intl.message(
      'Cancel',
      name: 'cancel',
    );
  String get add => Intl.message(
      'Add',
      name: 'add',
    );
  String get deletedFavorite => Intl.message(
      'Deleted Favorite',
      name: 'deletedFavorite',
    );
  String get undo => Intl.message(
      'Undo',
      name: 'undo',
    );
  String get storeUpToFavorites => Intl.message(
      'You can only store up to 6 favorites',
      name: 'storeUpToFavorites',
    );
  String get name => Intl.message(
      'Name',
      name: 'name',
    );
  String get leave => Intl.message(
      'Leave now',
      name: 'leave',
    );
  String get yes => Intl.message(
      'Yes',
      name: 'yes',
    );
  String get leaveConfirm => Intl.message(
      'Do you want to leave now?',
      name: 'leaveConfirm',
    );
  String get light => Intl.message(
      'Light',
      name: 'light',
    );
  String get dark => Intl.message(
      'Dark',
      name: 'dark',
    );
  String get snooze => Intl.message(
      'Snooze',
      name: 'snooze',
    );
  String get sessionEnd => Intl.message(
      'Your session is about to end',
      name: 'sessionEnd',
    );
  String get thankYou => Intl.message(
      'Thank you for choosing us today',
      name: 'thankYou',
    );
  String get ok => Intl.message(
      'OK',
      name: 'ok',
    );
    
   
  // String get xx => Intl.message(
  //     '',
  //     name: '',
  //     desc: '',
  //   );
}

class AppLocalizationDelegate extends LocalizationsDelegate<AppLocalization> {
  final Locale overriddenLocale;

  const AppLocalizationDelegate(this.overriddenLocale);

  @override
  bool isSupported(Locale locale) => [userdata.languageEN, userdata.languageDE, userdata.languageES].contains(locale.languageCode);

  @override
  Future<AppLocalization> load(Locale locale) => AppLocalization.load(locale);

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalization> old) => false;
}
